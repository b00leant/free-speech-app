# FreeSpeech

FreeSpeech is an opensource project born to help the transfeminist community against the gender-based violence

## The Collective

We are a bunch of friends that during the pandemic discovered an absence in digital and geospatial network for anti-violence centers, so we decided to create it. We were inspired by other associations like [Chayn](https://www.chayn.co/), [BeFree](https://www.befreecooperativa.org/) or [Direcontrolaviolenza](https://www.direcontrolaviolenza.it/), that make an execellent work for the feminist communities


## Open Data
FreeSpeech is an opensource app for the anti-violence centers networking based on open data, so if you want to help us in our reserarch [please mail us](mailto:freespeechcollective@hotmail.com?subject=[FreeSpeech]%20RFC) and provide us more informations about anti-violence centers, so that we preserve the most efficient and consistency in our database around the world 🌎

<br />
<div>
  &emsp;&emsp;&emsp;
  <img src="https://gitlab.com/b00leant/free-speech-app/-/raw/main/screenshots/map_android.png?ref_type=heads" alt="Map view" width="320">
  &emsp;&emsp;&emsp;&emsp;
  <img src="https://gitlab.com/b00leant/free-speech-app/-/raw/main/screenshots/list_android.png?ref_type=heads"" alt="List view" width="320">  
</div>
<br />

## Technology

The entire app is build using:
- Flutter 3.17.0-5.0.pre.63 
- Dart 3.3.0 (build 3.3.0-120.0.dev) • DevTools 2.30.0-dev.1

## Main features
- GIS navigation 
- Search

## Secondary features
- Shortcut to send emails to a center
- Shortcut to call a center
- Shortcut to your favourite map navigation app (Gmaps, Waze, Apple Maps etc…) 

## Upcoming features
- SOS feature with legal complaint automation
- REST API for anti-violence center research
- Internal rfc management for new centers

### Libraries & Tools Used

* [flutter_map](https://github.com/fleaflet/flutter_map)
* [geolocator](https://pub.dev/packages/geolocator)
* [introduction_screen](https://github.com/pyozer/introduction_screen)
* [package_info_plus](https://pub.dev/packages/package_info_plus)
* [flutter_map](https://github.com/flutterchina/dio)
* [Json Serialization](https://github.com/dart-lang/json_serializable)
* [Dependency Injection](https://github.com/fluttercommunity/get_it)
