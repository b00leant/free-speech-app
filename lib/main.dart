import 'package:app/av_center.dart';
import 'package:app/check_permissions.dart';
import 'package:app/const.dart';
import 'package:app/home.dart';
import 'package:app/routes.dart';
import 'package:app/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async{
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  try {
    await askPermissions();
    bool hasSplash = await getPrefsBool('hasSplash');
    List<AVCenter> listModel = [];
    listModel = await getPrefsList();
    if(listModel.isEmpty){
      listModel = await getAllCenters();
      await setPrefsList(listModel);
    }
    FlutterNativeSplash.remove();
    runApp(FreeSpeechApp(hasSplash: hasSplash));
  } catch (e) {
    FlutterNativeSplash.remove();
    runApp(const CheckPermissions());
  }
}


class FreeSpeechApp extends StatefulWidget {
  final bool hasSplash;
  const FreeSpeechApp({super.key, required this.hasSplash});

  @override
  State<FreeSpeechApp> createState() => _FreeSpeechAppState();
}


class _FreeSpeechAppState extends State<FreeSpeechApp> {
  bool permissionFailed = false;
  bool isDarkMode = false;

  @override
  void initState() {
    super.initState();
    setSplashFlag();
    initPlatformState();
    var brightness = SchedulerBinding.instance.platformDispatcher.platformBrightness;
    isDarkMode = brightness == Brightness.dark;
  }

  void setSplashFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('hasSplash', true);
  }

  var loading = true;
  late Coords currentCoords;

  //TODO future use for user tracking
  //String _deviceId = "";


  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    //String? deviceId;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      //deviceId = await PlatformDeviceId.getDeviceId;
    } on PlatformException {
      //deviceId = 'Failed to get deviceId.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      //_deviceId = deviceId!;
      //print("deviceId->$_deviceId");
    });
  }


  @override
  /// *
  /// [saveThemesOnChange] is used to save automatically the theme onChange
  /// [loadThemeOnInit] is used to load automatically the theme onInit
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Free Speech',
      theme: ThemeData(
        useMaterial3: true,
        primarySwatch: Colors.deepPurple,
        primaryColor: const Color(0xFF673AB7),
        primaryColorLight: Colors.deepPurple,
        primaryColorDark: Colors.black,
        // Define the default brightness and colors.
        colorScheme: ColorScheme.fromSeed(
          error: const Color(0xFFB71C1C),
          shadow: const Color(0xFF7D4ADE),
          primary: const Color(0xFF673AB7),
          seedColor: const Color(0xFF673AB7),
          secondary: const Color(0xFF673AB7),
          brightness: isDarkMode==true?Brightness.dark:Brightness.light,
        ),

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        //textTheme: TextTheme()

      ),
      initialRoute: widget.hasSplash ? '/home' : '/intro',
      routes: routes,
      home: loading==true?
      const Center(child: CircularProgressIndicator()):
      const HomePage(),
    );
  }
}