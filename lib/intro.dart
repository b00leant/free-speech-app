import 'package:app/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:introduction_screen/introduction_screen.dart';


class IntroPage extends StatefulWidget {

  const IntroPage({super.key});

  @override
  State<IntroPage> createState() => IntroPageState();
}

class IntroPageState extends State<IntroPage>{

  final introKey = GlobalKey<IntroductionScreenState>();
  bool ending = false;

  void _onIntroEnd(context) {
    goToHome(context);
  }

  /*
  Widget _buildImage(String assetName, [double width = 300]) {
    return Image.asset('assets/$assetName', width: width);
  }
  */

  @override
  Widget build(BuildContext context) {

    var pageDecoration = PageDecoration(
      titleTextStyle: Theme.of(context).textTheme.displaySmall!,//TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: Theme.of(context).textTheme.headlineSmall!,
      bodyPadding: const EdgeInsets.fromLTRB(20.0, 0.0, 26.0, 26.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return PopScope(
      canPop: false,
      //forbidden swipe in iOS(my ThemeData(platform: TargetPlatform.iOS,)
      /*
      onPopInvoked: (didPop) async {
        if (Navigator.of(context).userGestureInProgress) {
          return false;
        } else {
          return true;
        }
      },*/
      child: ending==true? const Center(child:CircularProgressIndicator()):IntroductionScreen(
        key: introKey,
        globalBackgroundColor: Colors.white,
        /*globalHeader: Align(
        alignment: Alignment.topRight,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: 16, right: 16,left: 16),
            child: FlutterLogo(),
          ),
        ),
      ),*/
        globalFooter: SizedBox(
          width: double.infinity,
          height: 60,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0.0),
                ),
                backgroundColor: Theme.of(context).colorScheme.primary,
                textStyle: TextStyle(
                  color: Colors.white,
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  fontSize: 20,
                )),
            child: const Text(
              'Ho capito',
              style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 20.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
            onPressed: () => _onIntroEnd(context),
          ),
        ),
        pages: [
          PageViewModel(
            title: "Benvenutə in \nFreeSpeech!",
            body: "Questo tutorial ti guiderà nell'uso di quest'applicazione",
            image: SvgPicture.asset(
              width:180,
              height:180,
        "assets/icon/logo.svg",
      ),
            decoration: pageDecoration.copyWith(
              bodyFlex: 1,
              imageFlex: 1,
              bodyAlignment: Alignment.center,
              imageAlignment: Alignment.bottomCenter,
              titleTextStyle: Theme.of(context).textTheme.headlineLarge?.copyWith(color: Colors.black),
              bodyTextStyle:Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,color: Colors.black),
            ),
          ),
          PageViewModel(
            title: "Scegli la modalità di ricerca",
            bodyWidget: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [const Icon(Icons.map,size: 70,color: Colors.black,),Text("Mappa",style: Theme.of(context).textTheme.headlineLarge?.copyWith(color: Colors.black))]),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: [const Icon(Icons.list,size: 70,color: Colors.black),Text("Lista",style: Theme.of(context).textTheme.headlineLarge?.copyWith(color: Colors.black))])
            ],),
            decoration: pageDecoration.copyWith(
              bodyFlex: 2,
              imageFlex: 4,
              bodyAlignment: Alignment.center,
              imageAlignment: Alignment.topCenter,
              titleTextStyle: Theme.of(context).textTheme.headlineLarge?.copyWith(color: Colors.black),
              bodyTextStyle:Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,color: Colors.black),
            ),
          ),
          PageViewModel(
            title: "Torna qui quando vuoi",
            body:
            "Potrai consultare questo tutorial dalla sezione 'Tutorial' dalla sezione 'About' dell'app!",
            image: null,//_buildFullscreenImage(),
            //image: null, //_buildImage('img2.jpg'),
            decoration: pageDecoration.copyWith(
              contentMargin: const EdgeInsets.symmetric(horizontal: 16),
              fullScreen: true,
              bodyFlex: 4,
              bodyAlignment: Alignment.center,
              imageAlignment: Alignment.center,
              titleTextStyle: Theme.of(context).textTheme.headlineLarge?.copyWith(color: Colors.black),
              bodyTextStyle:Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,color: Colors.black),
            ),
          ),
          PageViewModel(
            title: "Hai domande?",
            body: "",//"Contattaci via mail",
            image: null, //_buildImage('img2.jpg'),
            footer: Column(
              children: [
                /*ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    alignment: Alignment.center,
                    backgroundColor: Theme.of(context).colorScheme.primary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  onPressed: () {
                    //goToWebsite();
                  },
                  icon:
                  const Icon(Icons.public), //icon data for elevated button
                  label: const Text("Vai al sito"), //label text
                ),*/
                ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    alignment: Alignment.center,
                    backgroundColor: Theme.of(context).colorScheme.primary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  onPressed: () {
                    sendEmail("freespeechcollective@hotmail.com");
                  },
                  icon: const Icon(Icons.email,color: Colors.white,size:25,), //icon data for elevated button
                  label: Text("Scrivici",
                    style: Theme.of(context).textTheme.headlineSmall?.copyWith(color: Colors.white)) //label text
                ),
              ],
            ),
            //decoration: pageDecoration,
            decoration: pageDecoration.copyWith(
              bodyFlex: 2,
              imageFlex: 4,
              bodyAlignment: Alignment.bottomCenter,
              imageAlignment: Alignment.topCenter,
              titleTextStyle: Theme.of(context).textTheme.headlineLarge?.copyWith(color: Colors.black),
              bodyTextStyle:Theme.of(context).textTheme.titleLarge?.copyWith(fontWeight: FontWeight.w400,color: Colors.black),
            ),
          ),
        ],
        onDone: () => _onIntroEnd(context),
        //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
        showSkipButton: false,
        skipOrBackFlex: 0,
        nextFlex: 0,
        showBackButton: true,
        //rtl: true, // Display as right-to-left
        back: Icon(
          Icons.arrow_back,
          color: Theme.of(context).colorScheme.primary,
        ),
        skip: const Text('Skip',
            style:
            TextStyle(fontFamily: 'Roboto', fontWeight: FontWeight.w600)),
        next: Icon(Icons.arrow_forward, color: Theme.of(context).colorScheme.primary),
        done: Icon(Icons.done, color: Theme.of(context).colorScheme.primary),
        /*const Text('Fine',
          style: TextStyle(
              color: Color(0xFF1F7E54),
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w600)),*/
        curve: Curves.fastLinearToSlowEaseIn,
        controlsMargin: const EdgeInsets.all(16),
        controlsPadding: const EdgeInsets.all(12.0),
        //const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
        dotsDecorator: const DotsDecorator(
          size: Size(10.0, 10.0),
          color: Color(0xFFBDBDBD),
          activeSize: Size(22.0, 10.0),
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          ),
        ),
        dotsContainerDecorator: const ShapeDecoration(
          color: Colors.black87,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
        ),
      ),
    );
  }

  void goToHome(BuildContext context) async {
    setState(() {
      ending = true;
    });
    Navigator.of(context).pushReplacement(createHomeRoute());
  }

}