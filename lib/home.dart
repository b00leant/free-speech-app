import 'package:app/av_center.dart';
import 'package:app/list.dart';
import 'package:app/map.dart';
import 'package:app/search_delegate.dart';
import 'package:app/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:package_info_plus/package_info_plus.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  @override
  State<HomePage> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  late PackageInfo packageInfo;
  bool loading = true;
  String version = "";
  List<AVCenter> avList = [];
  late Coords currentCords;
  int _selectedIndex = 0;

  @override
  initState() {
    getPackageInfo();
    getData();
    super.initState();
  }

  getPackageInfo() async {
    packageInfo = await PackageInfo.fromPlatform();
    String versionAux = "V ${packageInfo.version} (${packageInfo.buildNumber})";
    setState(() {
      version = versionAux;
    });
  }

  Future<void> getData() async {
    await getCordsFromDisk();
  }

  getCordsFromDisk() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    currentCords = Coords(position.latitude, position.longitude);
    setState(() {
      loading = false;
    });
  }

  Widget getVew(int index) {
    if (index == 0) {
      return const MapPage();
    }
    return const ListPage();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<Widget> createAppBarActions(BuildContext context,
      [List<IconButton>? extra]) {
    List<Widget> ex = [];
    return [
      /*IconButton(
          onPressed: () {
            Navigator.of(context).push(_createFilterRoute());
          }
          , icon: const Icon(Icons.filter_list_alt)),*/
      IconButton(
          icon: const Icon(Icons.search),
          onPressed: () {
            showSearch(context: context, delegate: DataSearch(currentCords));
          }),
      ...ex
      /*if(extra!.isNotEmpty)...[
    ...extra
  ] else ...[

  ]*/
    ];
  }

  _infoDialogBuilder(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AboutDialog(
          applicationName: "FreeSpeech",
          applicationVersion: version,
          children: [
            SvgPicture.asset(
              width: 100,
              height: 100,
              "assets/icon/logo.svg",
            ),
            const SizedBox(height: 20),
            const Text(
                "È un collettivo nato nel 2020 nei pressi di Roma con l'intenzione "
                "di fornire uno strumento informativo alla lotta contro la violenza di genere.\n\n"
                "Quest'app visualizza la rete dei Centri Anti-Violenza nazionali.\n\n"
                "FreeSpeech ha scelto di non tollera Centri Anti-violenza TERF,\n"
                "se sei a conoscenza di un CAV TERF, ti preghiamo di segnalarcelo presso la nostra email!"),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('Mostra Tutorial'),
                    SizedBox(width: 10),
                    Icon(Icons.school_outlined)
                  ]),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed('/intro');
              },
            ),
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('Contattaci'),
                    SizedBox(width: 10),
                    Icon(Icons.email_outlined)
                  ]),
              onPressed: () {
                sendEmail("freespeechcollective@hotmail.com");
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
        canPop: false,
        child:loading == true
        ? Center(
            child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const CircularProgressIndicator(),
                      const SizedBox(height: 30),
                      Text(
                        "Caricamento dati in corso",
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(
                            color: Theme.of(context).colorScheme.primary),
                        textAlign: TextAlign.center,
                      )
                    ])))
        : Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: const Icon(Icons.info_outline),
                  onPressed: () => _infoDialogBuilder(context)),
              backgroundColor: Theme.of(context).colorScheme.inversePrimary,
              actions: createAppBarActions(context),
              title: const Text(''),
            ),
            body: getVew(_selectedIndex),
            bottomNavigationBar: BottomNavigationBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.map),
                  label: 'Mappa',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.list),
                  label: 'Lista',
                ),
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: Theme.of(context).colorScheme.primary,
              onTap: _onItemTapped,
            ),
          )
    );
  }
}
