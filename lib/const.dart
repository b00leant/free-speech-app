import "package:flutter/material.dart";
import "package:flutter/services.dart" as s;
import "package:yaml/yaml.dart";

Future<String> getBaseUrl() async {
  final data = await s.rootBundle.loadString('config/env.yaml');
  final mapData = loadYaml(data);
  String apiBase = mapData['apiBase'];
  //String apiPort = mapData['apiPort'];
  //String baseUrl = "$apiBase:$apiPort";
  String baseUrl = apiBase;
  return baseUrl;
}
// used for localhost testing
//const String baseUrl = 'http://10.0.2.2:3000';

const shimmerGradient = LinearGradient(
  colors: [
    Color(0xFFEBEBF4),
    //Color(0xFFF4F4F4),
    Color(0xFFCBB6F3),
    Color(0xFFEBEBF4),
  ],
  stops: [
    0.1,
    0.3,
    0.7,
  ],
  begin: Alignment(-1.0, -0.3),
  end: Alignment(1.0, 0.3),
  tileMode: TileMode.repeated,
);