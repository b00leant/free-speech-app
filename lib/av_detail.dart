import 'package:app/av_center.dart';
import 'package:app/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:map_launcher/map_launcher.dart';

class AVDetailPage extends StatelessWidget {
  final AVCenter cav;
  final Coords currentCoords;
  final DirectionsMode directionsMode;
  final List<Waypoint> waypoints = [
    //Waypoint(37.7705112, -122.4108267),
    // Coords(37.6988984, -122.4830961),
    // Coords(37.7935754, -122.483654),
  ];

  AVDetailPage({super.key, required this.cav,required this.currentCoords,required this.directionsMode});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: Theme.of(context).colorScheme.primary,
            statusBarIconBrightness:
                Brightness.dark, // For Android (dark icons)
            statusBarBrightness: Brightness.light, // For iOS (dark icons)
          ),
          leading: IconButton(
              icon: const Icon(Icons.chevron_left_sharp, color: Colors.white),
              onPressed: () {
                Navigator.of(context).pop();
              }),
          backgroundColor: Theme.of(context).colorScheme.primary),
      body: ListView(
        children: <Widget>[
          Container(
            height: 140,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Theme.of(context).colorScheme.primary,
                  Colors.deepPurple.shade700
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: const [0.5, 0.9],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.all(10),
                    child:Text(
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    cav.name,
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .headlineSmall
                        ?.copyWith(color: Colors.white))),
                /*Text(
                  'Flutter Developer',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),*/
                const SizedBox(
                  height: 10,
                ),
                /*
                Container(child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.red.shade300,
                        minRadius: 35.0,
                        child: Icon(
                          Icons.call,
                          size: 30.0,
                        ),
                      ),CircleAvatar(
                        backgroundColor: Colors.red.shade300,
                        minRadius: 35.0,
                        child: const Icon(
                          Icons.call,
                          size: 30.0,
                        ),
                      ),
                      CircleAvatar(
                        backgroundColor: Colors.red.shade300,
                        minRadius: 35.0,
                        child: Icon(
                          Icons.message,
                          size: 30.0,
                        ),
                      )])),
                */
              ],
            ),
          ),
          const SizedBox(height: 8),
        for (int i = 0; i < cav.phones.length; i++) ...[
    if (cav.phones[i].number != "") ...[
    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
    ListTile(
    trailing: RawMaterialButton(
    onPressed: () {
    phoneCall(cav.phones[i].number);
    },
    elevation: 1.0,
    fillColor: Theme.of(context).colorScheme.primary,
    padding: const EdgeInsets.all(15.0),
    shape: const CircleBorder(),
    child: const Icon(
    Icons.call,
    color: Colors.white,
    ),
    ),
    title: Text(cav.phones[i].number,
    overflow: TextOverflow.ellipsis,
    style: Theme.of(context)
        .textTheme
        .bodyLarge
        ?.copyWith(color: Colors.black)))
    ]),
    const SizedBox(height: 8),
    ] else
    ...[]
    ],
    for (int i = 0; i < cav.emails.length; i++) ...[
    if (cav.emails[i].address != "") ...[
    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
    ListTile(
    trailing: RawMaterialButton(
    onPressed: () {
    sendEmail(cav.emails[i].address);
    },
    elevation: 1.0,
    fillColor: Theme.of(context).colorScheme.primary,
    padding: const EdgeInsets.all(15.0),
    shape: const CircleBorder(),
    child: const Icon(
    Icons.email_outlined,
    color: Colors.white,
    ),
    ),
    title: Text(cav.emails[i].address,
    overflow: TextOverflow.ellipsis,
    style: Theme.of(context)
        .textTheme
        .bodyLarge
        ?.copyWith(color: Colors.black)))
    ]),
    const SizedBox(height: 8),
    ] else
    ...[]
    ],
    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
    ListTile(
    trailing: RawMaterialButton(
    onPressed: () {
    gpsNavigation(context, cav, currentCoords, waypoints,
    directionsMode);
    },
    elevation: 1.0,
    fillColor: Theme.of(context).colorScheme.primary,
    padding: const EdgeInsets.all(15.0),
    shape: const CircleBorder(),
    child: const Icon(
    Icons.navigation,
    color: Colors.white,
    ),
    ),
    title: Text("${cav.city}, ${cav.address}, ${cav.cap}",
    maxLines: 2,
    overflow: TextOverflow.ellipsis,
    style: Theme.of(context)
        .textTheme
        .bodyLarge
        ?.copyWith(color: Colors.black)))
    ]),
    const SizedBox(height: 8),
    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
    if (cav.website != "") ...[
    ListTile(
    trailing: RawMaterialButton(
    onPressed: () {
    goToHTTPS(cav.website);
    },
    elevation: 1.0,
    fillColor: Theme.of(context).colorScheme.primary,
    padding: const EdgeInsets.all(15.0),
    shape: const CircleBorder(),
    child: const Icon(
    Icons.link,
    color: Colors.white,
    ),
    ),
    title: Text(cav.website,
    maxLines: 2,
    overflow: TextOverflow.ellipsis,
    style: Theme.of(context)
        .textTheme
        .bodyLarge
        ?.copyWith(color: Colors.black)))
    ] else
    ...[],

          /*
          for (int i = 0; i < cav.phones.length; i++) ...[
            Card(
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              ListTile(
                  trailing: const Icon(Icons.call),
                  title: Text(
                    cav.phones[i].number,
                    /*
                    trailing: GestureDetector(
                      child: const Icon(Icons.chevron_right),
                      onTap: () =>,
                    */
                  ))
            ])),
            const SizedBox(height: 8),
          ],
          for (int i = 0; i < cav.emails.length; i++)
            Card(
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              ListTile(
                  trailing: const Icon(Icons.alternate_email),
                  title: Text(
                    cav.emails[i].address,
                    /*
                            trailing: GestureDetector(
                              child: const Icon(Icons.chevron_right),
                              onTap: () =>,
                            */
                  ))
            ])),
          const SizedBox(height: 8),
          Card(
              child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            ListTile(
              trailing: const Icon(Icons.navigation),
              title: Text("${cav.city}, ${cav.address}, ${cav.cap}"),
              /*
                            trailing: GestureDetector(
                              child: const Icon(Icons.chevron_right),
                              onTap: () =>,
                            */
            )
              */
              ]
              ),
          /*
          Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: Colors.deepOrange.shade300,
                    child: ListTile(
                      title: Text(
                        '5000',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          color: Colors.white,
                        ),
                      ),
                      subtitle: Text(
                        'Followers',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white70,
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    color: Colors.red,
                    child: ListTile(
                      title: Text(
                        '5000',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          color: Colors.white,
                        ),
                      ),
                      subtitle: Text(
                        'Following',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white70,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          */
        ],
      ),
    );
  }
}
