import 'package:app/av_center.dart';
import 'package:app/const.dart';
import 'package:app/list_item.dart';
import 'package:app/shimmer.dart';
import 'package:app/shimmer_loading.dart';
import 'package:app/utils.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class ListPage extends StatefulWidget {
  const ListPage({super.key});

  @override
  ListPageState createState() => ListPageState();
}

class ListPageState extends State<ListPage> {
  bool networkError = false;
  List<AVCenter> listModel = [];
  List<String> items = [];
  var loading = true;
  Coords currentCords = Coords(0, 0);

  getDataFromDisk() async {
    Future<SharedPreferences> instance = SharedPreferences.getInstance();
    dynamic ins = await instance;
    if (ins.getString("list") == null) {
    } else {
      String decodedList = ins.getString("list");
      List<AVCenter> listFromDisk = modelAVCenterFromJson(decodedList);
      setState(() {
        listModel = listFromDisk;
        loading = false;
      });
    }
  }

  Future<Null> getData() async {
    setState(() {
      loading = true;
    });
    await getDataFromDisk();
    Position geoCurrentPositon = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    currentCords =
        Coords(geoCurrentPositon.latitude, geoCurrentPositon.longitude);
    setState(() {
      loading = false;
    });
  }

  Future<Null> updateData(BuildContext context) async {
    setState(() {
      networkError = false;
      loading = true;
    });
    Position geoCurrentPositon = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    currentCords =
        Coords(geoCurrentPositon.latitude, geoCurrentPositon.longitude);
    try{
      listModel = await getAllCenters();
      await setPrefsList(listModel);
    } catch(e) {
      setState(() {
        networkError = true;
      });
    }
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    items = List<String>.generate(10000, (i) => 'Item $i');
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Theme.of(context).colorScheme.surfaceVariant,
        child:
            /*loading
            ? Center(
                child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Theme.of(context).colorScheme.primary)))
            : */
            RefreshIndicator(
                color: Colors.white,
                backgroundColor: Theme.of(context).colorScheme.primary,
                onRefresh: () async {
                  // Replace this delay with the code to be executed during refresh
                  // and return asynchronous code
                  //await Future.delayed(const Duration(seconds: 4));
                  return await updateData(context); // Future<void>.delayed(const Duration(seconds: 3));
                },
                // This check is used to customize listening to scroll notifications
                // from the widget's children.
                //
                // By default this is set to `notification.depth == 0`, which ensures
                // the only the scroll notifications from the first scroll view are listened to.
                //
                // Here setting `notification.depth == 1` triggers the refresh indicator
                // when overscrolling the nested scroll view.
                /*notificationPredicate: (ScrollNotification notification) {
                  return notification.depth == 0;
                },*/
                child: Stack(
                  children: [
                    !loading
                        ? ListView.builder(
                            shrinkWrap: false,
                            physics: const AlwaysScrollableScrollPhysics(),
                            /*const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),*/
                            itemCount: listModel.length,
                            itemBuilder: (context, i) {
                              return AVListItem(
                                  cav: listModel[i],
                                  currentCoordinates: currentCords);
                            })
                        : Shimmer(
                        linearGradient: shimmerGradient,
                        child: ListView(children: [
                          _buildListLoadingItem(),
                          _buildListLoadingItem(),
                          _buildListLoadingItem(),
                          _buildListLoadingItem(),
                          _buildListLoadingItem(),
                          _buildListLoadingItem(),
                          _buildListLoadingItem(),
                          _buildListLoadingItem(),
                          _buildListLoadingItem(),
                        ])),
                    if(networkError==true)...[Positioned(

                        child: Container(
                            width: MediaQuery.of(context).copyWith().size.width,
                            height: 30,
                            color: Colors.red[700],
                            child: Center(child:Text("Errore di comunicazione con il server",style: TextStyle(color: Colors.white),))))] else...[]
                  ],
                )
            )
    );
  }

  Widget _buildListLoadingItem() {
    return ShimmerLoading(
      isLoading: loading,
      child: loadingItem()
    );
  }
}
