import 'package:app/av_center.dart';
import 'package:app/utils.dart';
import 'package:flutter/material.dart';
import 'package:map_launcher/map_launcher.dart';

class CavBottomDialog extends StatelessWidget {
  final Coords currentCoords;
  final DirectionsMode directionsMode;
  final AVCenter cav;
  final List<Waypoint> waypoints = [
    //Waypoint(37.7705112, -122.4108267),
    // Coords(37.6988984, -122.4830961),
    // Coords(37.7935754, -122.483654),
  ];

  CavBottomDialog(
      {super.key,
      required this.cav,
      required this.currentCoords,
      required this.directionsMode});

  topBorderDecoration(BuildContext context) {
    return BoxDecoration(
      border: Border(
          top: BorderSide(
              color: Theme.of(context).colorScheme.primary, width: 3.0)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          color: Colors.transparent,
            border: Border(
                left: BorderSide( //                   <--- left side
                  color: Colors.deepPurple,
                  width: 4.0,
                ),
                right: BorderSide( //                   <--- left side
                  color: Colors.deepPurple,
                  width: 4.0,
                ),

            ), borderRadius: BorderRadius.only(topLeft: Radius.circular(25),topRight: Radius.circular(25))
        ),
        height: MediaQuery.of(context).copyWith().size.height * 0.80,
        //decoration: topBoderDecoration(context), //
        child: Column(
          children: <Widget>[
                Container(
                  height: 100,
                  width: MediaQuery.of(context).copyWith().size.width,
                  decoration: BoxDecoration(
                    color: Colors.deepPurple,
                    borderRadius: const BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(20)),
                    gradient: LinearGradient(
                      colors: [
                        Theme.of(context).colorScheme.primary,
                        Colors.deepPurple//.shade700
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: const [0.5, 0.9],
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.all(10),
                      child:Text(
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          cav.name,
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .headlineSmall
                              ?.copyWith(color: Colors.white))),
                    ],
                  ),
                ),
            const SizedBox(height: 20),
            for (int i = 0; i < cav.phones.length; i++) ...[
              if (cav.phones[i].number != "") ...[
                Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  ListTile(
                      trailing: RawMaterialButton(
                        onPressed: () {
                          phoneCall(cav.phones[i].number);
                        },
                        elevation: 1.0,
                        fillColor: Theme.of(context).colorScheme.primary,
                        padding: const EdgeInsets.all(15.0),
                        shape: const CircleBorder(),
                        child: const Icon(
                          Icons.call,
                          color: Colors.white,
                        ),
                      ),
                      title: Text(cav.phones[i].number,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge
                              ?.copyWith(color: Colors.black)))
                ]),
                const SizedBox(height: 8),
              ] else
                ...[]
            ],
            for (int i = 0; i < cav.emails.length; i++) ...[
              if (cav.emails[i].address != "") ...[
                Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  ListTile(
                      trailing: RawMaterialButton(
                        onPressed: () {
                          sendEmail(cav.emails[i].address);
                        },
                        elevation: 1.0,
                        fillColor: Theme.of(context).colorScheme.primary,
                        padding: const EdgeInsets.all(15.0),
                        shape: const CircleBorder(),
                        child: const Icon(
                          Icons.email_outlined,
                          color: Colors.white,
                        ),
                      ),
                      title: Text(cav.emails[i].address,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge
                              ?.copyWith(color: Colors.black)))
                ]),
                const SizedBox(height: 8),
              ] else
                ...[]
            ],
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              ListTile(
                  trailing: RawMaterialButton(
                    onPressed: () {
                      gpsNavigation(context, cav, currentCoords, waypoints,
                          directionsMode);
                    },
                    elevation: 1.0,
                    fillColor: Theme.of(context).colorScheme.primary,
                    padding: const EdgeInsets.all(15.0),
                    shape: const CircleBorder(),
                    child: const Icon(
                      Icons.navigation,
                      color: Colors.white,
                    ),
                  ),
                  title: Text("${cav.city}, ${cav.address}, ${cav.cap}",
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: Colors.black)))
            ]),
            const SizedBox(height: 8),
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              if (cav.website != "") ...[
                ListTile(
                    trailing: RawMaterialButton(
                      onPressed: () {
                        goToHTTPS(cav.website);
                      },
                      elevation: 1.0,
                      fillColor: Theme.of(context).colorScheme.primary,
                      padding: const EdgeInsets.all(15.0),
                      shape: const CircleBorder(),
                      child: const Icon(
                        Icons.link,
                        color: Colors.white,
                      ),
                    ),
                    title: Text(cav.website,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context)
                            .textTheme
                            .bodyLarge
                            ?.copyWith(color: Colors.black)))
              ] else
                ...[],
            ])
          ],
        ));
  }
}
