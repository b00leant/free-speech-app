import 'package:app/check_permissions.dart';
import 'package:app/home.dart';
import 'package:app/intro.dart';
import 'package:app/list.dart';
import 'package:app/map.dart';
import 'package:flutter/cupertino.dart';

///
/// constant that defines the routes of Woofer app
final Map<String,WidgetBuilder> routes = <String, WidgetBuilder>
{
  '/map': (context) =>  const MapPage(),
  '/list': (context) => const ListPage(),
  '/home': (context) => const HomePage(),
  '/intro': (context) => const IntroPage(),
  '/permissions': (context) => const CheckPermissions(),
};