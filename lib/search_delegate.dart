import 'dart:convert';

import 'package:app/av_center.dart';
import 'package:app/av_detail.dart';
import 'package:app/const.dart';
import 'package:app/list_item.dart';
import 'package:app/utils.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:map_launcher/map_launcher.dart';

///
/// [DataSearch] class is a custom [SearchDelegate]
/// used to enhance the search purposes inside Woofer
///
/// - [cavList] is a [List] containing all the suggestions
/// that will be showed, at each tap corresponds a navigation
/// through [DetailView]
class DataSearch extends SearchDelegate<String> {
  Coords currentCoords;
  DataSearch(this.currentCoords);
  List<AVCenter> listModel = [];
  DirectionsMode directionsMode = DirectionsMode.driving;
  List<AVCenter> suggestionListModel = [];

  Future<List<AVCenter>> _search(String query) async {
    List<AVCenter> searchResults = [];
    String baseUrl = await getBaseUrl();
    final uri = Uri.parse('$baseUrl/search?q=$query');
    final responseData = await http.get(uri);
    if (responseData.statusCode == 200) {
      final data = jsonDecode(responseData.body);
      for (Map<String, dynamic> i in data) {
        searchResults.add(AVCenter.fromJson(i));
      }
    }
    return searchResults;
  }

  Future<List<AVCenter>> _suggest(String query) async {
    List<AVCenter> diskResult = await getPrefsList();
    suggestionListModel = diskResult.where((cav) {
      return cav.name.contains(RegExp(query, caseSensitive: false)) ||
          cav.city.contains(RegExp(query, caseSensitive: false)) ||
          cav.address.contains(RegExp(query, caseSensitive: false));
    }).toList();

    return suggestionListModel;
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            query = '';
          })
    ];
  }

  @override
  String get searchFieldLabel => 'Ricerca CAV';

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
        onPressed: () {
          close(context, "");
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // old local query
    /*final List<AVCenter> searchResults = listModel
        .where(
            (item) {
              return item.name.contains(RegExp(query, caseSensitive: false)) ||
                  item.city.contains(RegExp(query, caseSensitive: false)) ||
                  item.address.contains(RegExp(query, caseSensitive: false)) ||
                  item.region.contains(RegExp(query, caseSensitive: false));
            })
        .toList();

     */
    return FutureBuilder<List<AVCenter>>(
        future: _search(query),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return AVListItem(cav: snapshot.data![index],currentCoordinates: currentCoords);
                /*ListTile(
                  onTap: () {
                    Navigator.of(context)
                        .push(_createDetailRoute(snapshot.data![index]));
                  },
                  trailing: const Icon(Icons.chevron_right),
                  title: Text(snapshot.data![index].name),
                );*/
              },
              itemCount: snapshot.data?.length,
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  /// helper to have better rendered texts
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  @override
  Widget buildSuggestions(BuildContext context) {
    //searchData(query);
    /*
    final suggestionList = query.isEmpty

        ? listModel
        : listModel
            .where((p) => p.name.contains(RegExp(query, caseSensitive: false)))
            .toList();
    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
        onTap: () {
          Navigator.of(context).push(_createDetailRoute(suggestionList[index]));
        },
        trailing: const Icon(Icons.chevron_right),
        title: buildRichText(context, suggestionList[index], query),
      ),
      itemCount: suggestionList.length,
    );
    */

    return FutureBuilder<List<AVCenter>>(
        future: _suggest(query),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return ListView.builder(
              itemBuilder: (context, index) => ListTile(
                onTap: () {
                  Navigator.of(context).push(_createDetailRoute(suggestionListModel[index]));
                },
                trailing: const Icon(Icons.chevron_right),
                title: buildRichText(context, suggestionListModel[index], query),
              ),
              itemCount: suggestionListModel.length,
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  Route _createDetailRoute(AVCenter cav) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => AVDetailPage(currentCoords: currentCoords,
        cav: cav, directionsMode: directionsMode,
      ),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(1.0, 0.0);
        const end = Offset.zero;
        const curve = Curves.ease;
        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  RichText buildRichText(BuildContext context, AVCenter item, String query) {
    /*tem.name.runes.forEach((int rune) {
      var character= String.fromCharCode(rune);
      print(character);
    });*/

    /*
    for (int i = 0; i < item.name.length; i++) {
      var char = item.name[i];
    }*/

    int indexOf = item.name.toLowerCase().indexOf(query.toLowerCase());
    if (indexOf < 0) {
      return RichText(
          text: TextSpan(
              text: capitalize(item.name),
              style: const TextStyle(color: Colors.grey),
              children: const []));
    } else {
      return RichText(
        text: TextSpan(
          text: (item.name).substring(0, indexOf),
          style: const TextStyle(color: Colors.grey),
          children: [
            TextSpan(
              text: (item.name).substring(indexOf, indexOf + query.length),
              style: TextStyle(
                  color: Theme.of(context).colorScheme.primary,
                  fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: capitalize(item.name)
                        .substring(indexOf + query.length, item.name.length),
                    style: const TextStyle(color: Colors.grey))
              ],
            )
          ],
        ),
      );
    }
  }
}
