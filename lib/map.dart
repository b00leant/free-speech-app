import 'dart:async';
import 'package:app/av_center.dart';
import 'package:app/cav_bottom_dialog.dart';
import 'package:app/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_map/flutter_map.dart' as lmap;
import 'package:map_launcher/map_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  MapPageState createState() => MapPageState();
}

class MapPageState extends State<MapPage> {
  bool isDark = false;
  DirectionsMode directionsMode = DirectionsMode.driving;
  bool networkError = false;
  late Position geoCurrentPositon;
  late LatLng initialCenter;
  List<AVCenter> listModel = [];
  var loading = true;
  List<lmap.Marker> markers = [];
  MapController mapController = MapController();
  final minLatLng = const LatLng(49.8566, 1.3522);
  final maxLatLng = const LatLng(58.3498, -10.2603);
  late LocationMarkerPosition currentPosition;

  getData() async {
    await getLocation();
    await getDataFromDisk();
    await drawMarkers(listModel);
    if (mounted) {
      setState(() {
        loading = false;
      });
    }
  }

  Future<Null> updateData(BuildContext context) async {
    if (mounted) {
      setState(() {
        networkError = false;
        loading = true;
      });
    }
    markers = [];
    listModel = [];
    await getLocation();
    try {
      listModel = await getAllCenters();
    } catch (e) {
      setState(() {
        networkError = true;
      });
    }
    await setPrefsList(listModel);
    await drawMarkers(listModel);
    if (mounted) {
      setState(() {
        loading = false;
      });
    }
  }

  getLocation() async {
    geoCurrentPositon = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    initialCenter =
        LatLng(geoCurrentPositon.latitude, geoCurrentPositon.longitude);
    currentPosition = LocationMarkerPosition(
      latitude: geoCurrentPositon.latitude,
      longitude: geoCurrentPositon.longitude,
      accuracy: geoCurrentPositon.accuracy,
    );
  }

  getDataFromDisk() async {
    Future<SharedPreferences> instance = SharedPreferences.getInstance();
    dynamic ins = await instance;
    if (ins.getString("list") != null) {
      String decodedList = ins.getString("list");
      List<AVCenter> listFromDisk = modelAVCenterFromJson(decodedList);
      listModel = listFromDisk;
    } else {
      listModel = await getAllCenters();
    }
  }

  drawMarkers(List<AVCenter> list) async {
    for (final cav in list) {
      if (cav.lat != "" && cav.lng != "") {
        if (mounted) {
          final marker = Marker(
              height: 30,
              width: 30,
              point: LatLng(double.parse(cav.lat), double.parse(cav.lng)),
              child: GestureDetector(
                  onTap: () {
                    Coords currentCoords = Coords(geoCurrentPositon.latitude,
                        geoCurrentPositon.longitude);
                    showModalBottomSheet<void>(
                      isScrollControlled: true,
                      context: context,
                      builder: (BuildContext context) {
                        return CavBottomDialog(
                            cav: cav,
                            currentCoords: currentCoords,
                            directionsMode: directionsMode);
                      },
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Theme.of(context).colorScheme.primary,
                    ),
                    child: const Center(
                      child: Icon(Icons.home, color: Colors.white),
                    ),
                  )));
          markers.add(marker);
        }
      }
    }
  }

  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            body: Stack(children: [
          if (loading == true) ...[
            Container(
                color: Theme.of(context).colorScheme.surfaceVariant,
                child: const Center(child: CircularProgressIndicator()))
          ] else ...[
            FlutterMap(
              mapController: mapController,
              options: MapOptions(
                initialCenter: initialCenter,
                initialZoom: 5.8,
                maxZoom: 15,
              ),
              children: [
                TileLayer(
                  urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                  subdomains: const ['a', 'b', 'c'],
                ),
                /*CurrentLocationLayer(
                    //followScreenPoint: const Point(0.0, 1.0),
                    //followScreenPointOffset: const Point(0.0, 1.0),
                    followCurrentLocationStream: _followCurrentLocationStreamController.stream,
                    turnHeadingUpLocationStream: _turnHeadingUpStreamController.stream,
                    followOnLocationUpdate: _followOnLocationUpdate,
                    turnOnHeadingUpdate: _turnOnHeadingUpdate,
                    style: const LocationMarkerStyle(
                      marker: DefaultLocationMarker(
                        child: Icon(
                          Icons.navigation,
                          color: Colors.white,
                        ),
                      ),
                      markerSize: Size(40, 40),
                      markerDirection: MarkerDirection.heading,
                    ),
                  ),*/
                AnimatedLocationMarkerLayer(
                  position: currentPosition,
                ),
                MarkerClusterLayerWidget(
                  options: MarkerClusterLayerOptions(
                    spiderfyCircleRadius: 80,
                    spiderfySpiralDistanceMultiplier: 2,
                    circleSpiralSwitchover: 12,
                    maxClusterRadius: 120,
                    rotate: true,
                    size: const Size(40, 40),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(50),
                    maxZoom: 16,
                    markers: markers,
                    builder: (context, markers) {
                      return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Theme.of(context).colorScheme.primary,
                        ),
                        child: Center(
                          child: Text(
                            markers.length.toString(),
                            style: const TextStyle(color: Colors.white),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: FloatingActionButton(
                        backgroundColor: Theme.of(context).colorScheme.primary,
                        onPressed: () {
                          updateData(context);
                          // Follow the location marker on the map when location updated until user interact with the map.
                          //setState(
                          //      () => _followOnLocationUpdate = FollowOnLocationUpdate.always,
                          //);
                          // Follow the location marker on the map and zoom the map to level 18.
                          //_followCurrentLocationStreamController.add(18);
                        },
                        child: const Icon(
                          Icons.refresh,
                          color: Colors.white,
                        )),
                  ),
                ),
              ],
            )
          ],
          /*
          Positioned(child: Container(
            margin: const EdgeInsets.only(top: 50),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SearchAnchor(
                  builder: (BuildContext context, SearchController controller) {
                    return SearchBar(
                      controller: controller,
                      padding: const MaterialStatePropertyAll<EdgeInsets>(
                          EdgeInsets.symmetric(horizontal: 16.0)),
                      onTap: () {
                        controller.openView();
                      },
                      onChanged: (_) {
                        controller.openView();
                      },
                      leading: Icon(Icons.search),
                      trailing: <Widget>[
                        Tooltip(
                          message: 'Change brightness mode',
                          child: IconButton(
                            isSelected: isDark,
                            onPressed: () {
                              setState(() {
                                isDark = !isDark;
                              });
                            },
                            icon: const Icon(Icons.wb_sunny_outlined),
                            selectedIcon: const Icon(Icons.brightness_2_outlined),
                          ),
                        )
                      ],
                    );
                  }, suggestionsBuilder:
                  (BuildContext context, SearchController controller) {
                return List<ListTile>.generate(listModel.length, (int index) {
                  final String item = listModel[index].name;
                  return ListTile(
                    title: Text(item),
                    onTap: () {
                      setState(() {
                        controller.closeView(item);
                      });
                    },
                  );
                });
              }),
            ),
          ))
            */
          if (networkError == true) ...[
            Positioned(
                child: Container(
                    width: MediaQuery.of(context).copyWith().size.width,
                    height: 30,
                    color: Colors.red[700],
                    child: Center(
                        child: Text(
                      "Errore di comunicazione con il server",
                      style: TextStyle(color: Colors.white),
                    ))))
          ] else
            ...[]
        ])));
  }
}
