import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:logger/logger.dart';

class CheckPermissions extends StatefulWidget {
  const CheckPermissions({super.key});
  @override
  State<CheckPermissions> createState() => CheckPermissionsState();
}

class CheckPermissionsState extends State<CheckPermissions> {
  bool tryAsk = false;
  var logger = Logger();
  askPermissions() async {
    try {
      LocationPermission permission;
      bool serviceEnabled;
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        return Future.error('Location services are disabled.');
      }

      permission = await Geolocator.checkPermission();
      permission = await Geolocator.requestPermission();

      if (permission == LocationPermission.denied) {
        tryAsk = true;
        return Future.error('Location permissions are denied');
      }


      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }
    }
    catch (e) {
      int i = 1;
    }
  }

  checkPermissions() async {
    try {
      LocationPermission permission;
      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        tryAsk = true;
        return Future.error('Location permissions are denied');
      }
      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }
    }
    catch (e) {
      logger.d(e);
    }
  }

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Free Speech',
        theme: ThemeData(
          useMaterial3: true,
          primarySwatch: Colors.deepPurple,
          primaryColor: const Color(0xFF673AB7),
          primaryColorLight: Colors.deepPurple,
          primaryColorDark: Colors.black,
          // Define the default brightness and colors.
          colorScheme: ColorScheme.fromSeed(
            error: const Color(0xFFB71C1C),
            shadow: const Color(0xFF7D4ADE),
            primary: const Color(0xFF673AB7),
            seedColor: const Color(0xFF673AB7),
            secondary: const Color(0xFF673AB7),
            brightness: Brightness.light,
          ),

          // Define the default `TextTheme`. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          //textTheme: TextTheme()
        ),
        home: Center(
            child: Padding(
                padding: const EdgeInsets.all(50),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.gps_off,
                          color: Theme.of(context).colorScheme.primary,
                          size: 50),
                      const SizedBox(height: 30),
                      Text(
                        "FreeSpeech ha bisogno dei permessi di localizzazione per funzionare correttamente\n\n${tryAsk==true?"Chidei i permessi":"Modifica le impostazioni, e riavvia l'app"}\n\n",
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(
                            color: Theme.of(context).colorScheme.primary),
                        textAlign: TextAlign.center,
                      ),
                      tryAsk==true?ElevatedButton(
                          onPressed: askPermissions,
                          child: const Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                            Text("Chiedi permessi di localizzazione"),
                            Icon(Icons.gps_fixed)
                          ])):ElevatedButton(
                          onPressed: () => AppSettings.openAppSettings(),
                          child: const Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text("Impostazioni FreeSpeech"),
                                Icon(Icons.settings)
                              ]))
                    ]))));
  }
}
