import 'dart:convert';

List<EmailContact> modelEmailContactListFromJson(String str) =>
    List<EmailContact>.from(
        json.decode(str).map((x) => EmailContact.fromJson(x)));

class EmailContact {
  String address;
  EmailContact({required this.address});
  factory EmailContact.fromJson(Map<String, dynamic> json) =>
      EmailContact(address: json["address"]);
  Map<String, dynamic> toJson() => {"address": address};
}

List<PhoneContact> modelPhoneContactListFromJson(String str) =>
    List<PhoneContact>.from(
        json.decode(str).map((x) => PhoneContact.fromJson(x)));

class PhoneContact {
  String number;
  PhoneContact({required this.number});
  factory PhoneContact.fromJson(Map<String, dynamic> json) =>
      PhoneContact(number: json["number"]);
  Map<String, dynamic> toJson() => {"number": number};
}
