import 'dart:convert';

import 'package:app/av_center.dart';
import 'package:app/const.dart';
import 'package:app/filter.dart';
import 'package:app/home.dart';
import 'package:app/map_sheet.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:theme_provider/theme_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

String? encodeQueryParameters(Map<String, String> params) {
  return params.entries
      .map((MapEntry<String, String> e) =>
          '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
      .join('&');
}

sendEmail(String email) async {
  final Uri httpsLaunchUri = Uri(
    scheme: 'mailto',
    path: email,
    query: encodeQueryParameters(<String, String>{
      'subject': '',
    }),
  );
  if (!await launchUrl(httpsLaunchUri)) {
    throw Exception('Could not launch $httpsLaunchUri');
  }
}

goToHTTPS(String url) async {
  final Uri httpsLaunchUri = Uri(
    scheme: 'https',
    path: url,
  );
  if (!await launchUrl(httpsLaunchUri)) {
    throw Exception('Could not launch $httpsLaunchUri');
  }
}

Route createFilterRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => const Filter(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(1.0, 0.0);
      const end = Offset.zero;
      const curve = Curves.ease;
      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

Route createHomeRoute() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => const HomePage(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(1.0, 0.0);
      const end = Offset.zero;
      const curve = Curves.ease;
      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

gpsNavigation(BuildContext context, AVCenter cav, Coords currentCoords,
    List<Waypoint> waypoints, DirectionsMode directionsMode) {
  MapsBottomSheet.show(
    context: context,
    onMapTap: (map) {
      map.showDirections(
        destination: Coords(
          cav.latLng.latitude,
          cav.latLng.longitude,
        ),
        destinationTitle: cav.name,
        origin: currentCoords,
        originTitle: "",
        waypoints: waypoints,
        directionsMode: directionsMode,
      );
    },
  );
}

phoneCall(String phoneNumber) async {
  final Uri phoneLaunchUri = Uri(
    scheme: 'tel',
    path: phoneNumber,
  );
  if (!await launchUrl(phoneLaunchUri)) {
    throw Exception('Could not launch $phoneNumber');
  }
}

Future<Null> askPermissions() async {
  bool serviceEnabled;
  LocationPermission permission;
  // Test if location services are enabled.

  serviceEnabled = await Geolocator.isLocationServiceEnabled();

  if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the
    // App to enable the location services.
    return Future.error('Location services are disabled.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }

  return;
}

setPrefsList(List<AVCenter> listModel) async {
  Future<SharedPreferences> instance = SharedPreferences.getInstance();
  dynamic ins = await instance;
  String jsonList = jsonEncode(listModel);
  await ins.setString('list', jsonList);
}

Future<List<AVCenter>> getPrefsList() async {
  Future<SharedPreferences> instance = SharedPreferences.getInstance();
  dynamic ins = await instance;
  if (ins.getString("list") != null) {
    String decodedList = ins.getString("list");
    List<AVCenter> listFromDisk = modelAVCenterFromJson(decodedList);
    return listFromDisk;
  } else {
    return [];
  }
}

Future<List<AVCenter>> getAllCenters() async {
  List<AVCenter> res = [];
  String baseUrl = await getBaseUrl();
  final url = Uri.parse('$baseUrl/av_centers');
  final responseData = await http.get(url);
  if (responseData.statusCode == 200) {
    final data = jsonDecode(responseData.body);
    for (Map<String, dynamic> i in data) {
      res.add(AVCenter.fromJson(i));
    }
  } else {
    return Future.error("Network error code: ${responseData.statusCode}");
  }
  return Future.value(res);
}

setPrefsCoords(Position currentPosition) async {
  Future<SharedPreferences> instance = SharedPreferences.getInstance();
  dynamic ins = await instance;
  await ins.setDouble('lat', currentPosition.latitude);
  await ins.setDouble('lng', currentPosition.longitude);
}

Future<bool> getPrefsBool(String value) async {
  Future<SharedPreferences> instance = SharedPreferences.getInstance();
  dynamic ins = await instance;
  dynamic res = Null;
  if (ins.getBool(value) == null) {
    res = false;
  } else {
    var i = ins.getBool(value);
    res = i;
  }
  return Future.value(res);
}

/// function that returns the list of the app AppTheme objects
///
/// edit [themes] if you want to add more themes or change the
/// dark/light alternatives
List<AppTheme> getAppThemes(BuildContext context) {
  List<AppTheme> themes = [
    AppTheme(
        description: "lightTheme",
        id: "light",
        data: ThemeData(
          brightness: Brightness.light,
          primaryColor: Theme.of(context).colorScheme.primary,
          secondaryHeaderColor: Colors.white,
          fontFamily: 'Arial',
          colorScheme: ColorScheme.fromSeed(seedColor: const Color(0xFF673AB7)),
          useMaterial3: true,
        )),
    AppTheme(
        description: "darkTheme",
        id: "dark",
        data: ThemeData(
          brightness: Brightness.dark,
          primaryColor: Theme.of(context).colorScheme.primary,
          secondaryHeaderColor: Colors.white,
          fontFamily: 'Arial',
          useMaterial3: true,
        ))
  ];
  return themes;
}

Widget loadingItem() {
  return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 3),
      child: Container(
          constraints: BoxConstraints(
              minHeight: 140, minWidth: double.infinity, maxHeight: 200),
          child: Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.place),
                  title: Text("Centro tal dei tali"),
                  subtitle: Text("Città, Via Roma, 666"),
                ),
                //Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    TextButton(
                      child: const Row(
                        children: [
                          Icon(Icons.navigation),
                          //Text("INDICAZIONI")
                        ],
                      ),
                      onPressed: () {},
                    ),
                    TextButton(
                      child: Row(
                        children: [
                          const Icon(Icons.phone),
                          //Text("CHIAMA")
                        ],
                      ),
                      onPressed: () {},
                    ),
                    TextButton(
                      child: const Row(children: [
                        Icon(Icons.insert_link),
                      ]),
                      onPressed: () {},
                    ),
                    TextButton(
                      child: Row(
                        children: [
                          const Icon(Icons.email_outlined),
                        ],
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
              ],
            ),
          )
      )
  );
}
