import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Filter extends StatefulWidget {
  const Filter({super.key});

  @override
  State<Filter> createState() => FilterState();
}

class FilterState extends State<Filter> {
  bool checkboxValueBed = false;
  bool checkboxValueListen = false;
  bool checkboxValueTherapy = false;
  double kmRangeValue = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: const Icon(Icons.chevron_left_sharp, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: const Text('Filtro', style: TextStyle(color: Colors.white)),
      ),
      body: Center(
          child: ListView(
        children: [
          CheckboxListTile(
              title: const Row(
                children: [
                  Icon(Icons.bed),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Posti letto")
                ],
              ),
              value: checkboxValueBed,
              onChanged: (value) {
                setState(() {
                  checkboxValueBed = !checkboxValueBed;
                });
                if (kDebugMode) {
                  print("checkbox value is $value");
                }
              }),
          CheckboxListTile(
              title: const Row(
                children: [
                  Icon(Icons.hearing),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Sportello")
                ],
              ),
              value: checkboxValueListen,
              onChanged: (value) {
                setState(() {
                  checkboxValueListen = !checkboxValueListen;
                });
                if (kDebugMode) {
                  print("checkbox value is $value");
                }
              }),
          CheckboxListTile(
              title: const Row(
                children: [
                  Icon(Icons.psychology),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Psicoterapia")
                ],
              ),
              value: checkboxValueTherapy,
              onChanged: (value) {
                setState(() {
                  checkboxValueTherapy = !checkboxValueTherapy;
                });
                if (kDebugMode) {
                  print("checkbox value is $value");
                }
              }),
          const ListTile(
              title: Row(children: [
            Icon(Icons.share_location_sharp),
            SizedBox(width: 10),
            Text("Distanza"),
          ])),
          const SizedBox(height: 40),
          Slider(
            value: kmRangeValue,
            max: 100,
            divisions: 5,
            label: kmRangeValue.round().toString(),
            onChanged: (double value) {
              setState(() {
                kmRangeValue = value;
              });
            },
          ),
          ElevatedButton.icon(
              onPressed: () => {Navigator.of(context).pop()},
              icon: const Icon(Icons.done),
              label: const Text("Applica"))
        ],
      )),
    );
  }
}
