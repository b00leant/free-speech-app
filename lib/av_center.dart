import 'dart:convert';
import 'package:app/contact.dart';
import 'package:latlong2/latlong.dart' as llng;

List<AVCenter> modelUserFromJson(String str) =>
    List<AVCenter>.from(json.decode(str).map((x) => AVCenter.fromJson(x)));

List<AVCenter> modelAVCenterFromJson(String str) =>
    List<AVCenter>.from(json.decode(str).map((x) => AVCenter.fromJson(x)));

List<EmailContact> parseEmailsModel(List<dynamic> emailContacts) {
  List<EmailContact> results = [];
  for (final e in emailContacts) {
    EmailContact ec = EmailContact(address: e['address']);
    results.add(ec);
  }
  return results;
}

List<PhoneContact> parsePhonesModel(List<dynamic> phonesContacts) {
  List<PhoneContact> results = [];
  for (final e in phonesContacts) {
    PhoneContact pc = PhoneContact(number: e['number']);
    results.add(pc);
  }
  return results;
}

List<EmailContact> parseEmails(List<dynamic> emailContacts) {
  //List<String> results = [];
  List<EmailContact> results = [];
  for (final e in emailContacts) {
    if (e["address"] != "") {
      EmailContact ec = EmailContact(address: e['address']);
      results.add(ec);
    }
  }
  return results;
}

List<PhoneContact> parsePhones(List<dynamic> phonesContacts) {
  //List<String> results = [];
  List<PhoneContact> results = [];
  for (final e in phonesContacts) {
    if (e["number"] != "") {
      PhoneContact pc = PhoneContact(number: e['number']);
      results.add(pc);
    }
  }
  return results;
}

class AVCenter {
  String id;
  String name;
  String cap;
  List<EmailContact> emails;
  String lat;
  String lng;
  String address;
  List<PhoneContact> phones;
  String website;
  String city;
  String region;

  llng.LatLng latLng;

  AVCenter(
      {required this.id,
      required this.name,
      required this.cap,
      required this.emails,
      required this.address,
      required this.phones,
      required this.website,
      required this.region,
      required this.city,
      required this.lat,
      required this.lng,
      required this.latLng});

  factory AVCenter.fromJson(Map<String, dynamic> json) => AVCenter(
        id: json["id"],
        name: json["name"],
        cap: json["postalCode"],
        emails: parseEmails(json[
            'email_contacts']), //modelEmailContactListFromJson(json['email_contacts']),//parseEmails(json["email_contacts"]),
        phones: parsePhones(json[
            'phone_contacts']), //modelPhoneContactListFromJson(json['phone_contacts']),//parsePhones(json["phone_contacts"]),
        address: json["address"],
        website: json["website"],
        region: json["region"],
        city: json["city"],
        lat: json["lat"],
        lng: json["lng"],
        latLng: (json["lat"] != null && json["lng"] != null)
            ? llng.LatLng(double.parse(json["lat"]), double.parse(json["lng"]))
            : const llng.LatLng(0, 0),
      );
  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "postalCode": cap,
        "email_contacts": emails,
        "address": address,
        "phone_contacts": phones,
        "website": website,
        "region": region,
        "city": city,
        "lat": lat,
        "lng": lng,
        "latLng": latLng
      };

  llng.LatLng get location => latLng;
}
