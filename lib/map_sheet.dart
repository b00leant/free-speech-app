import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:map_launcher/map_launcher.dart';

class MapsBottomSheet {
  static show({
    required BuildContext context,
    required Function(AvailableMap map) onMapTap,
  }) {
    getMapsAsync().then((availableMaps) {
      showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: SingleChildScrollView(
                    child: Wrap(
                      children: <Widget>[
                        for (var map in availableMaps)
                          ListTile(
                            onTap: () => onMapTap(map),
                            title: Text(map.mapName),
                            leading: SvgPicture.asset(
                              map.icon,
                              height: 40.0,
                              width: 40.0,
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );
    });
  }

  static Future<List<AvailableMap>> getMapsAsync() async{
    List<AvailableMap> res = await MapLauncher.installedMaps;
    return res;
}
}