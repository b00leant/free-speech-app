import 'package:app/av_center.dart';
import 'package:app/utils.dart';
import 'package:flutter/material.dart';
import 'package:map_launcher/map_launcher.dart';

class AVListItem extends StatelessWidget {
  final AVCenter cav;
  final Coords currentCoordinates;
  final DirectionsMode directionsMode = DirectionsMode.driving;
  // List<Coords> waypoints = [];

  final List<Waypoint> waypoints = [
    //Waypoint(37.7705112, -122.4108267),
    // Coords(37.6988984, -122.4830961),
    // Coords(37.7935754, -122.483654),
  ];

  AVListItem({super.key, required this.cav, required this.currentCoordinates});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 3),
        child: Container(
            constraints: BoxConstraints(
                minHeight: 140, minWidth: double.infinity, maxHeight: 200),
            child: Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: const Icon(Icons.place),
                    title: Text(cav.name),
                    subtitle: Text("${cav.city}, ${cav.address}, ${cav.cap}"),
                    /*
            trailing: GestureDetector(
              child: const Icon(Icons.chevron_right),
              onTap: () =>,
            */
                  ),
                  //Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        child: const Row(
                          children: [
                            Icon(Icons.navigation),
                            //Text("INDICAZIONI")
                          ],
                        ),
                        onPressed: () {
                          gpsNavigation(context, cav, currentCoordinates,
                              waypoints, directionsMode);
                        },
                      ),
                      if (cav.phones.isEmpty) ...[
                        const SizedBox(width: 8)
                      ] else ...[
                        TextButton(
                          child: Row(
                            children: [
                              if (cav.phones.length > 1) ...[
                                const Icon(Icons.phone)
                              ] else ...[
                                const Icon(Icons.settings_phone)
                              ],
                              //Text("CHIAMA")
                            ],
                          ),
                          onPressed: () {
                            phoneCall(cav.phones.first.number);
                          },
                        )
                      ],
                      if (cav.website.isEmpty && cav.website != "") ...[
                        const SizedBox(width: 8)
                      ] else ...[
                        TextButton(
                          child: const Row(
                            children: [
                              Icon(Icons.insert_link),
                              //Text("CHIAMA")
                            ],
                          ),
                          onPressed: () {
                            goToHTTPS(cav.website);
                          },
                        )
                      ],
                      if (cav.emails.isEmpty) ...[
                        const SizedBox(width: 8)
                      ] else ...[
                        TextButton(
                          child: Row(
                            children: [
                              if (cav.emails.length > 1) ...[
                                const Icon(Icons.email_outlined)
                              ] else ...[
                                const Icon(Icons.email_outlined)
                              ],
                              //Text("CHIAMA")
                            ],
                          ),
                          onPressed: () {
                            sendEmail(cav.emails.first.address);
                          },
                        )
                      ],
                      /*const SizedBox(width: 8),
              TextButton(
                child: const Row(children: [
                  Icon(Icons.info),
                  //Text("CHIAMA")
                ],),
                onPressed: () {
                  Navigator.of(context).push(_createDetailRoute(cav));
                },
              )
               */
                    ],
                  ),
                ],
              ),
            )));
  }
}
